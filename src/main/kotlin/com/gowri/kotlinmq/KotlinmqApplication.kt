package com.gowri.kotlinmq

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.jms.annotation.EnableJms

@SpringBootApplication
@EnableJms
class KotlinmqApplication

fun main(args: Array<String>) {
    runApplication<KotlinmqApplication>(*args)
}
