package com.gowri.kotlinmq.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jms.JmsException
import org.springframework.jms.annotation.JmsListener
import org.springframework.jms.core.JmsTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/**

 * Created by Aham on 12/10/19.

 */
@RestController
class MqController(
    @Autowired
    private var jmsTemplate: JmsTemplate

)   {
    companion object{
        private val logger = LoggerFactory.getLogger(MqController::class.java)
    }
    @GetMapping("send")
    fun send() : String{

        try {
            jmsTemplate.convertAndSend("DEV.QUEUE.1", "Hello World!")
        }catch (ex: JmsException){
            logger.error("error sending message to mq")
        }


        logger.info("successfully posted message to MQ" )
        return "OK"
    }

    @JmsListener(destination = "DEV.QUEUE.1")
    fun receive(text: String){
        logger.info("message recieved on mq $text")
    }
    
}